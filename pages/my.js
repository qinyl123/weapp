"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    tel: '400-888-8888',
    userInfo: Object
  },
  methods: {},
  onLoad: function onLoad() {
    wx.navigateTo({
      url: '/pages/login'
    });
  }
}, {info: {"components":{"van-cell":{"path":"./../components/lib/cell/index"}},"on":{}}, handlers: {}, models: {}, refs: undefined });