"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    value: ''
  },
  methods: {
    submit: function submit() {
      if (!this.value) return;
      console.log(this.value);
    }
  },
  onLoad: function onLoad(option) {}
}, {info: {"components":{"van-button":{"path":"./../components/lib/button/index"}},"on":{"17-0":["tap"]}}, handlers: {'17-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {'0': {
      type: "input",
      expr: "value",
      handler: function set ($v) {
      var _vm=this;
        _vm.value = $v;
      
    }
    }}, refs: undefined });