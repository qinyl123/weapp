"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    page: 1,
    list: [],
    tel: '400-8888-888'
  },
  methods: {
    doorList: function doorList() {
      var _this2 = this;

      _http["default"].request(_config["default"].url.doorList, {
        page: this.page,
        limit: 10
      }).then(function (res) {
        _this2.list = res.data;
        console.log(res);
      });
    },
    // onReachBottom() { 
    //   this.page = this.page ? this.page + 1 : 1
    //   console.log(this)
    //   console.log(this.page)
    //   this.doorList()
    //   console.log('上拉加载')
    // },
    getRoute: function getRoute() {
      wx.navigateTo({
        url: '/pages/doorDetail'
      });
    },
    showModal: function showModal(e) {
      var _this = this;

      wx.showModal({
        title: '提示',
        content: "\u76EE\u524D\u533B\u751F\u4E0A\u95E8\u670D\u52A1\u9700\u901A\u8FC7\u7535\u8BDD".concat(_this.tel, "\u7535\u8BDD\u9884\u7EA6"),
        confirmText: '拨打',
        confirmColor: '#28B9D1',
        success: function success(res) {
          if (res.confirm) {
            wx.makePhoneCall({
              phoneNumber: _this.tel
            });
          }
        }
      });
    }
  },
  onLoad: function onLoad() {
    this.doorList();
  }
}, {info: {"components":{"van-tab":{"path":"./../components/lib/tab/index"},"van-tabs":{"path":"./../components/lib/tabs/index"}},"on":{"15-0":["disabled"]}}, handlers: {'15-0': {"disabled": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.showModal($event);
      })();
    
  }},'15-1': {"tap": function proxy (item) {
    
    var _vm=this;
      return (function () {
        _vm.getRoute(item.group);
      })();
    
  }}}, models: {}, refs: undefined });