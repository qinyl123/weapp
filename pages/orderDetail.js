"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _store = _interopRequireDefault(require('./../store/index.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    orderDetail: {},
    isOrder: true
  },
  methods: {
    submit: function submit() {
      console.log('取消预约');
    }
  },
  onLoad: function onLoad(option) {
    console.log(_store["default"]);
    this.orderDetail = _store["default"].state.orderDetail;
    this.id = option.id;
    option.index === "0" ? this.isOrder = true : this.isOrder = false;
  }
}, {info: {"components":{"van-cell":{"path":"./../components/lib/cell/index"},"van-button":{"path":"./../components/lib/button/index"},"van-cell-group":{"path":"./../components/lib/cell-group/index"}},"on":{}}, handlers: {'13-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.submit($event);
      })();
    
  }}}, models: {}, refs: undefined });