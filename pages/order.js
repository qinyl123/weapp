"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

var _store = _interopRequireDefault(require('./../store/index.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    index: "0",
    list: [],
    doorList: []
  },
  methods: {
    toPage: function toPage(item) {
      _store["default"].state.orderDetail = item;
      wx.navigateTo({
        url: "/pages/orderDetail?index=".concat(this.index)
      });
    },
    getList: function getList(url) {
      var _this = this;

      _http["default"].request(url, {
        page: 1,
        limit: 10
      }).then(function (res) {
        console.log(res);

        if (_this.index == 0) {
          _this.list = res.data;
        } else {
          _this.doorList = res.data;
        }
      });
    },
    onChange: function onChange(event) {
      this.index = event.$wx.detail[1].index;

      if (this.index == 0) {
        this.getList(_config["default"].url.shopDoctorList);
      } else {
        this.getList(_config["default"].url.doorServiceList);
      } //doorServiceList

    }
  },
  onLoad: function onLoad() {
    console.log(_store["default"]);
    this.getList(_config["default"].url.shopDoctorList);
  }
}, {info: {"components":{"van-tab":{"path":"./../components/lib/tab/index"},"van-tabs":{"path":"./../components/lib/tabs/index"},"order-list":{"path":"./../components/order-list"}},"on":{"11-0":["onChange"],"11-1":["getRoute"],"11-2":["getRoute"]}}, handlers: {'11-0': {"onChange": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.onChange($event);
      })();
    
  }},'11-1': {"getRoute": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.toPage($event);
      })();
    
  }},'11-2': {"getRoute": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.toPage($event);
      })();
    
  }}}, models: {}, refs: undefined });