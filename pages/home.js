"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  config: {
    navigationBarTitleText: '首页'
  },
  mixins: [],
  data: {
    address: '福建省龙海市海澄镇埭新村下仓2号',
    list: []
  },
  computed: {},
  methods: {
    onReachBottom: function onReachBottom() {
      console.log('onReachBottom');
    }
  },
  onLoad: function onLoad() {
    var _this = this;

    _http["default"].request(_config["default"].url.homeList, {
      page: '1',
      limit: 10
    }).then(function (res) {
      _this.list = res.data;
      console.log(res);
    });
  }
}, {info: {"components":{"card":{"path":"./../components/card"}},"on":{}}, handlers: {}, models: {}, refs: undefined });