"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {},
  methods: {
    bindGetUserInfo: function bindGetUserInfo(e) {
      console.log(e.$wx.detail.userInfo);
      var userInfo = e.$wx.detail.userInfo;
      wx.login({
        success: function success(res) {
          if (res.code) {
            //发起网络请求
            wx.request({
              url: _config["default"].url.authorization,
              method: 'POST',
              header: {
                'content-type': 'application/json' // 默认值

              },
              data: {
                code: res.code,
                name: userInfo.nickName,
                avatar: userInfo.avatarUrl
              },
              success: function success(res) {
                console.log(res);
                var data = res.data;

                if (data.code == 200) {
                  wx.navigateBack(-1);
                } else {
                  wx.showToast({
                    title: res.data.message,
                    icon: 'none',
                    duration: 2000
                  });
                }
              }
            });
          } else {
            console.log('登录失败！' + res.errMsg);
          }
        }
      });
    }
  },
  onLoad: function onLoad() {}
}, {info: {"components":{},"on":{}}, handlers: {'8-0': {"getuserinfo": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.bindGetUserInfo($event);
      })();
    
  }}}, models: {}, refs: undefined });