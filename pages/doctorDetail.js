"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

var _http = _interopRequireDefault(require('./../service/http.js'));

var _config = _interopRequireDefault(require('./../service/config.js'));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].page({
  data: {
    id: '',
    activeNames: [],
    info: Object,
    morning: Number,
    Afternoon: Number
  },
  methods: {
    selectDate: function selectDate(item) {
      this.morning = item.morning;
      this.Afternoon = item.Afternoon;
    },
    submit: function submit() {
      console.log('取消预约');
    },
    onChange: function onChange(event) {
      console.log(event);
      this.activeNames = event.$wx.detail[1];
    }
  },
  onLoad: function onLoad(option) {
    var _this = this;

    this.id = option.id;

    _http["default"].request(_config["default"].url.appointment, {}, 'GET').then(function (res) {
      console.log(res);
      _this.info = res.data;
    });
  }
}, {info: {"components":{"van-cell":{"path":"./../components/lib/cell/index"},"van-button":{"path":"./../components/lib/button/index"},"van-collapse":{"path":"./../components/lib/collapse/index"},"van-collapse-item":{"path":"./../components/lib/collapse-item/index"},"card":{"path":"./../components/card"}},"on":{"10-0":["change"]}}, handlers: {'10-0': {"change": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.onChange($event);
      })();
    
  }},'10-1': {"tap": function proxy (item) {
    
    var _vm=this;
      return (function () {
        _vm.selectDate(item);
      })();
    
  }}}, models: {}, refs: undefined });