"use strict";

var _core = _interopRequireDefault(require('./../vendor.js')(0));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_core["default"].component({
  props: {
    isHome: {
      type: Boolean,
      "default": false
    },
    detail: Object
  },
  methods: {
    getRoute: function getRoute() {
      wx.navigateTo({
        url: '/pages/doctorDetail'
      });
    }
  }
}, {info: {"components":{"van-button":{"path":"./lib/button/index"}},"on":{"38-0":["tap"]}}, handlers: {'38-0': {"tap": function proxy () {
    var $event = arguments[arguments.length - 1];
    var _vm=this;
      return (function () {
        _vm.getRoute($event);
      })();
    
  }}}, models: {}, refs: undefined });