class Point {
  constructor() {
  }
}

class http extends Point {
  constructor() {
    super();
  }
  request(url, params, method = 'POST') {
    return new Promise((resolve, rejects) => {
      wx.request({
        url: url, //仅为示例，并非真实的接口地址
        data: params,
        method: method,
        header: {
          'content-type': 'application/json' // 默认值
        },
        success(res) {
          return resolve(res.data)
        }
      })
    })
  }
}

export default new http()
