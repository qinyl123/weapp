const testUrl = 'http://zensuo.winderinfo.com/index.php' // 测试环境
const devUrl = 'http://fkjk.xznsyh.com' // 线上环境
const isdev = 0 //true 线上环境 false测试环境
const ajaxUrl = isdev ? devUrl : testUrl

export default {
  url: {
    authorization: `${ajaxUrl}/api/auth/authorization`, // 授权登录
    homeList: `${ajaxUrl}/api/index/index`, //首页
    doorList: `${ajaxUrl}/api/door/index`, //上门服务列表
    shopDoctorList: `${ajaxUrl}/api/index/emplacement_list`, //我的到店就诊
    doorServiceList: `${ajaxUrl}/api/door/emplacement_list`, //上门服务订单
    upload: `${ajaxUrl}/api/auth/upload`, // 上传
    identification: `${ajaxUrl}/api/door/identification`, // 选择上门服务确认
    appointment: `${ajaxUrl}/api/index/make_an_appointment`, // 医生详情
    
  }
}
